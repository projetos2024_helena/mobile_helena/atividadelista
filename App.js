import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UsersDetails from "./src/detailsUsers";
import ListUsers from "./src/listUsers";
import { useEffect, useState } from "react";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="List">
        <Stack.Screen name="List" component={ListUsers}></Stack.Screen>
        <Stack.Screen name="Details" component={UsersDetails}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
