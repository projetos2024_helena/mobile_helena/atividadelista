import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const UsersDetails = ({ route }) => {
  const { user } = route.params;

  return (
    <View>
      <Text>Detalhes da Tarefa:</Text>
      <Text>Nome: {user.name}</Text>
      <Text>Email: {user.email}</Text>
      <Text>Local: {user.address.city}</Text>
      <Text>Casa: {user.address.suite}</Text>
      <Text>Telefone: {user.address.zipcode}</Text>
      <Text>Celular: {user.phone}</Text>
      <Text>Site: {user.website}</Text>
      <Text>Empresa: {user.company.name}</Text>

      
    </View>
  );
};
export default UsersDetails;
