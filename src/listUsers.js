import React, { useEffect, useState } from "react";
import { View, Text, FlatList,TouchableOpacity } from "react-native";
import axios from "axios";

const ListUsers = ({ navigation }) => {

    const [users, setUsers] = useState([]);
    useEffect(() =>{
        getUsers();
    },[])

    async function getUsers(){
        try{
          const reponse = await axios.get("https://jsonplaceholder.typicode.com/users");
          setUsers(reponse.data);
        }catch(error){}
        console.error(error)
    }

    const taskPress = (user) => {
        navigation.navigate("Details", {user});
      };
    

  return (
    <View>
      <Text> Lista de Usuários </Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
            <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default ListUsers;